package com.smada;

import com.smada.model.GsrData;
import com.smada.service.DataService;
import com.smada.service.StatisticsService;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Command line runner of application
 */
public class GsrCommandLineRunner {

    /**
     * Main method
     *
     * @param args 1- input file, 2-output file, 3-bins amount
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        String inputPath = args[0];
        String outputPath = args[1];
        File outputFile = new File(outputPath);
        int part = 100;

        System.out.println("Input file: " + inputPath);

        if (args.length > 2) {
            try {
                part = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                System.out.println("Wrong value of bin numbers. Bin amount set to 100");
            }
        }

        File file = new File(args[0]);
        GsrData data = DataService.readFile(file);
        List<Float> result = StatisticsService.medianFilter(data.getValues(), part);
        result = StatisticsService.downsamplingBins(result, part);
        result = StatisticsService.normalize(result);
        GsrData processedData = new GsrData("id");
        processedData.setValues(result);
        DataService.saveFile(processedData, outputFile);
        System.out.println("Saved normalized data to: " + outputPath);
    }
}
