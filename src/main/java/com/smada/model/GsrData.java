package com.smada.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent input CSV file with measurement of GSR.
 */
public class GsrData {

    /**
     * Id of file.
     */
    String patientId;

    /**
     * {@link List} of values from data.
     */
    List<Float> values = new ArrayList<>();

    /**
     * Unit name
     */
    String unit;

    /**
     * Creates instance of {@link GsrData}.
     * @param patientId by {@link String}
     */
    public GsrData(String patientId) {
        this.patientId = patientId;
    }

    /**
     * Gets patient id.
     * @return patient id by {@link String}
     */
    public String getPatientId() {
        return patientId;
    }

    /**
     * Gets {@link List} of GSR measurement values.
     * @return results as {@link List}
     */
    public List<Float> getValues() {
        return values;
    }

    /**
     * Sets values for patient
     * @param values by {@link List} of float
     */
    public void setValues(List<Float> values) {
        this.values = values;
    }

    /**
     * Gets unit name
     * @return unit name
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets unit name
     * @param unit by {@link String}
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }
}
