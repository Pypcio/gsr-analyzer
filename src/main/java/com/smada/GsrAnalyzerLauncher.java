package com.smada;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML {@link Application} launcher
 */
public class GsrAnalyzerLauncher extends Application {

    /**
     * Root node
     */
    private Parent rootNode;

    /**
     * The FXML loader
     */
    private FXMLLoader fxmlLoader;

    @Override
    public void start(Stage primaryStage) throws Exception {
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/fxml/main.fxml"));
        rootNode = fxmlLoader.load();

        primaryStage.setTitle("GSR analysis");
        Scene scene = new Scene(rootNode, 1280, 720);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
