package com.smada;

import com.smada.model.GsrData;
import com.smada.service.DataService;
import com.smada.service.StatisticsService;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.*;

/**
 * Controller for FXML application.
 */
public class GsrController implements Initializable {

    /**
     * String for original data chart legend.
     */
    private static final String ORIGINAL_GSR = "original GSR";

    /**
     * String for normalized data chart legend.
     */
    private static final String NORMALIZED_SERIES_NAME = "Normalized, factor = ";

    /**
     * String for choose input data label.
     */
    private static final String CHOOSE_INPUT_DATA = "Choose input data";

    /**
     * String for slash sign.
     */
    private static final String SLASH = "/";

    /**
     * FXML {@link Pane} instance.
     */
    @FXML
    private AnchorPane panel;

    /**
     * FXML horizontal {@link NumberAxis} instance of chart.
     */
    @FXML
    private NumberAxis horizontalAxisOriginal = new NumberAxis();

    /**
     * FXML vertical {@link NumberAxis} instance of chart.
     */
    @FXML
    private NumberAxis verticalAxisOriginal = new NumberAxis();

    /**
     * FXML chart by {@link LineChart} to store original data.
     */
    @FXML
    private LineChart originalChart = new LineChart<>(horizontalAxisOriginal, verticalAxisOriginal);

    /**
     * FXML horizontal {@link NumberAxis} instance of chart.
     */
    @FXML
    private NumberAxis horizontalAxisNormalized = new NumberAxis();

    /**
     * FXML vertical {@link NumberAxis} instance of chart.
     */
    @FXML
    private NumberAxis verticalAxisNormalized = new NumberAxis();

    /**
     * FXML chart by {@link LineChart} to store normalized data.
     */
    @FXML
    private LineChart normalizedChart = new LineChart<>(horizontalAxisNormalized, verticalAxisNormalized);

    /**
     * Button to run process process.
     */
    @FXML
    private Button normalizeBtn;

    /**
     * Button to run process process.
     */
    @FXML
    private Button saveBtn;

    /**
     * {@link TextField} to store number of data elements to merge.
     */
    @FXML
    private TextField partField;

    /**
     * {@link TextField} to store output directory for save file.
     */
    @FXML
    private TextField outputDirLabel;

    /**
     * {@link TextField} to store output directory for save file.
     */
    @FXML
    private TextField outputFileLabel;

    /**
     * Original input data by {@link GsrData} .
     */
    private GsrData gsrData;

    /**
     * Normalized data by {@link GsrData} .
     */
    private GsrData processedGsrDate;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /**
     * Open file choose to read CSV file.
     */
    @FXML
    public void chooseInputFile() {
        clear();
        Optional<File> file = Optional.ofNullable(openFileChooser());
        file.ifPresent(this::fillData);
        if (gsrData != null) {
            drawOriginalData();
            normalizeBtn.setDisable(false);
        }
    }

    /**
     * Clears normalized chart.
     */
    public void clearNormalized() {
        normalizedChart.getData().clear();
        saveBtn.setDisable(true);
    }

    /**
     * Clears both charts.
     */
    @FXML
    public void clear() {
        if (gsrData != null) {
            gsrData = null;
            originalChart.getData().clear();
            normalizeBtn.setDisable(true);
            clearNormalized();
        }
    }

    /**
     * Process original data and draw plot with these values.
     */
    @FXML
    public void process() {
        if (partField.getText() != null && NumberUtils.isDigits(partField.getText())) {
            int part = Integer.parseInt(partField.getText());
            processedGsrDate = processData(gsrData.getValues(), part);
            String seriesName = NORMALIZED_SERIES_NAME + part;
            fillChart(normalizedChart, processedGsrDate.getValues(), seriesName, gsrData.getUnit());
            saveBtn.setDisable(false);
        }
    }

    /**
     * Saves normalized data to file.
     */
    @FXML
    public void saveToFile() {
        if (outputDirLabel.getText() != null && outputFileLabel.getText() != null) {
            try {
                File file = new File(new StringBuilder().append(outputDirLabel.getText()).append(SLASH).append(outputFileLabel.getText()).toString());
                DataService.saveFile(processedGsrDate, file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Sets output directory.
     */
    @FXML
    public void setOutputDir() {
        Optional<String> file = Optional.ofNullable(chooseDirectory());
        file.ifPresent(x -> outputDirLabel.setText(x));
    }

    private GsrData processData(List<Float> values, int part) {
        List<Float> result = StatisticsService.medianFilter(values, part);
        result = StatisticsService.downsamplingBins(result, part);
//        BAD IDEA. ALREADY IT downsample part times because replace all items in bin into single value
//        int additionalDownsamplingTimes = 2;
//        result = downsampling(result, additionalDownsamplingTimes);
//        result = doubleExponentialSmoothing(result, 0.6, 0.8, 0); JUST FOR FUN, it tries to smooth it. parameters need to be adjusted
        result = StatisticsService.normalize(result);
        GsrData gsrData = new GsrData(this.gsrData.getPatientId());
        gsrData.setValues(result);
        return gsrData;
    }

    public static List<Float> doubleExponentialSmoothing(List<Float> data, double alpha, double gamma, int initializationMethod) {
        double[] y = new double[data.size()];
        double[] s = new double[data.size()];
        double[] b = new double[data.size()];
        s[0] = y[0] = data.get(0);

        if(initializationMethod==0) {
            b[0] = data.get(1)-data.get(0);
        } else if(initializationMethod==1 && data.size()>4) {
            b[0] = (data.get(3) - data.get(0)) / 3;
        } else if(initializationMethod==2) {
            b[0] = (data.get(data.size() - 1) - data.get(0))/(data.size() - 1);
        }

        int i = 1;
        y[1] = s[0] + b[0];
        for (i = 1; i < data.size()-1; i++) {
            s[i] = alpha * data.get(i) + (1 - alpha) * (s[i - 1]+b[i - 1]);
            b[i] = gamma * (s[i] - s[i - 1]) + (1-gamma) * b[i-1];
            y[i+1] = s[i] + b[i];
        }
        return Arrays.stream(y).mapToObj(d -> (float)d).collect(Collectors.toList());
    }

    private void fillData(File file) {
        try {
            gsrData = DataService.readFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(CHOOSE_INPUT_DATA);
        return fileChooser.showOpenDialog(panel.getScene().getWindow());
    }

    private void drawOriginalData() {
        fillChart(originalChart, gsrData.getValues(), ORIGINAL_GSR, gsrData.getUnit());
    }

    private void fillChart(LineChart chart, List<Float> data, String seriesName, String unit) {
        chart.setCreateSymbols(false);
        chart.setLegendVisible(false);
        XYChart.Series currentSeries = new XYChart.Series();
        currentSeries.setName(seriesName);
        chart.getYAxis().setLabel(unit);

        for (int i = 0; i < data.size(); i++) {
            currentSeries.getData().add(new XYChart.Data(i, data.get(i)));
        }
        chart.getData().add(currentSeries);
        chart.setLegendVisible(true);
        chart.setLegendSide(Side.BOTTOM);
    }

    private String chooseDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Choose folder");
        String path = directoryChooser.showDialog(panel.getScene().getWindow()).getAbsolutePath();
        return path;
    }


}
