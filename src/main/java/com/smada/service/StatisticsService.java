package com.smada.service;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Service for statistics operations.
 */
public class StatisticsService {

    /**
     * Normalized list of results.
     *
     * @param result by {@link List} of floats
     * @return normalized {@link List}
     */
    public static List<Float> normalize(List<Float> result) {
        DoubleSummaryStatistics stats = result.stream()
                .mapToDouble(n -> (double) n)
                .summaryStatistics();
        return result.stream()
                .map(x -> (x - stats.getMin()) / (stats.getMax() - stats.getMin()))
                .map(Double::floatValue)
                .collect(Collectors.toList());
    }

    /**
     * Downsampling of bins based on given part parameter.
     *
     * @param result {@link List to downsampled}
     * @param part   of simple bin
     * @return {@link List} of data
     */
    public static List<Float> downsamplingBins(List<Float> result, int part) {
        return streamOfBins(result, part)
                .map(bin -> bin.get(0))
                .collect(Collectors.toList());
    }

    /**
     * Made medial filter of data based on given part value.
     *
     * @param values to be filtered
     * @param part   to calculated medial value
     * @return {@link List} of data
     */
    public static List<Float> medianFilter(List<Float> values, int part) {
        return streamOfBins(values, part)
                .flatMap(bin -> {
                    Float median = bin.stream().sorted().skip(bin.size() / 2).findFirst().get();
                    return Stream.generate(() -> median).limit(part).collect(Collectors.toList()).stream();
                })
                .collect(Collectors.toList());
    }

    /**
     * Creates stream of bins.
     *
     * @param values of data by {@link List}
     * @param part   amount
     * @return {@link Stream} of {@link List}
     */
    private static Stream<List<Float>> streamOfBins(List<Float> values, int part) {
        return IntStream.range(0, (values.size() + part - 1) / part)
                .mapToObj(i -> values.subList(i * part, Math.min(part * (i + 1), values.size())));
    }

}
