package com.smada.service;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.smada.model.GsrData;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Service to read data from input CSV file, based on {@link DataService}.
 */
public class DataService {

    /**
     * Default separator for one of given example
     */
    private static final String SEPARATOR = ";";

    /**
     * Reads CSV file to the application.
     * @param file input {@link File}
     * @return instance of {@link GsrData}
     * @throws IOException if file does not exist
     */
    public static GsrData readFile(File file) throws IOException {
        GsrData gsrData = new GsrData(file.getName());
        List<Float> values = new ArrayList<>();
        CSVReader reader = new CSVReader(new FileReader(file));
        reader.readNext(); // header
        String[] line;
        while ((line = reader.readNext()) != null) {
            if (line.length > 1) {
                float value = Float.parseFloat(line[1]);
                values.add(value);
                gsrData.setUnit("uS");
            } else {
                float value = Float.parseFloat(line[0].substring(line[0].indexOf(SEPARATOR) + 1));
                values.add(value);
                gsrData.setUnit("Ohm");
            }
        }
        gsrData.setValues(values);
        return gsrData;
    }

    /**
     * Saves {@link GsrData} to new file.
     * @param data of {@link GsrData} file
     * @param fileName to save file.
     * @throws IOException if file does not exist
     */
    public static void saveFile(GsrData data, File fileName) throws IOException {
        Writer writer = new FileWriter(fileName);
        CSVWriter csvWriter = new CSVWriter(writer);
        List<Float> dataToSave = data.getValues();
        for (float simpleData : dataToSave) {
            csvWriter.writeNext(new String[]{String.valueOf(simpleData)});
        }
        csvWriter.close();
    }

}
